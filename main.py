from fastapi import FastAPI, HTTPException
from typing import List
import mysql.connector
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET"],
    allow_headers=["*"],
)

conn = mysql.connector.connect(
    host="localhost",
    user="",
    password="",
    database="dbloteria",
    port="3306"
)
cursor = conn.cursor()

@app.get("/cards/", response_model=List[dict])
async def get_cards():
    try:
        cursor.execute("SELECT * FROM card")
        card = []
        for (id_card, name, image) in cursor:
            card.append({"id_card": id_card, "name": name, "image": image})
        return card
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

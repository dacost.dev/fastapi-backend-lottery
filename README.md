# API de Cartas de Lotería Mexicana con FastAPI

## Introducción

Esta API está construida utilizando FastAPI y proporciona un servicio para obtener cartas de la lotería mexicana.

## Instalación y Configuración

Para configurar la API en un entorno virtual en cualquier sistema operativo, sigue estos pasos:

1. Clona el repositorio en tu máquina local:

```
git clone git@gitlab.com:dacost.dev/fastapi-backend-lottery.git
```

2. Crea un entorno virtual. Si aún no has instalado `virtualenv`, puedes hacerlo usando pip:

```
pip install virtualenv
```

4. Luego, crea un entorno virtual llamado `venv`:

```
python3 -m virtualenv venv
```

5. Activa el entorno virtual. El comando de activación depende de tu sistema operativo:

   - Para Windows:

     ```
     venv\Scripts\activate
     ```

   - Para macOS y Linux:

     ```
     source venv/bin/activate
     ```

6. Ejecuta la API:

```
uvicorn main:app --reload
```

La API ahora debería estar funcionando localmente en `http://localhost:8000`.

## Base de Datos

El script de la base de datos se encuentra en la raíz del directorio del proyecto. Estos scripts fueron creados utilizando SQL para MySQL.

![Imagen 1](./model.png)
_SQL Model._

## Uso

Una vez que la API esté en funcionamiento, puedes acceder a la documentación de la API y probar los puntos de acceso navegando a `http://localhost:8000/docs` en tu navegador web.

## Licencia

Este proyecto está bajo la Licencia MIT. Para más detalles, consulta el archivo LICENSE.md.

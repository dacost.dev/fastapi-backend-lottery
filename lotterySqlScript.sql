SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `dbloteria` DEFAULT CHARACTER SET utf8 ;
USE `dbloteria` ;


CREATE TABLE IF NOT EXISTS `dbloteria`.`card` (
  `id_card` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `image` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id_card`),
  UNIQUE INDEX `id_cards_UNIQUE` (`id_card` ASC) VISIBLE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `dbloteria`.`player_table` (
  `id_player_table` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_player_table`),
  UNIQUE INDEX `id_player_table_UNIQUE` (`id_player_table` ASC) VISIBLE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `dbloteria`.`card_has_player_table` (
  `card_id_cards` INT NOT NULL,
  `player_table_id_player_table` INT NOT NULL,
  PRIMARY KEY (`card_id_cards`, `player_table_id_player_table`),
  INDEX `fk_card_has_player_table_player_table1_idx` (`player_table_id_player_table` ASC) VISIBLE,
  INDEX `fk_card_has_player_table_card_idx` (`card_id_cards` ASC) VISIBLE,
  CONSTRAINT `fk_card_has_player_table_card`
    FOREIGN KEY (`card_id_cards`)
    REFERENCES `dbloteria`.`card` (`id_card`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_card_has_player_table_player_table1`
    FOREIGN KEY (`player_table_id_player_table`)
    REFERENCES `dbloteria`.`player_table` (`id_player_table`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO card (name, image) VALUES
('El Gallo', 'https://i.pinimg.com/474x/c6/64/cd/c664cdcacd86ae135cdfd20afccb9cb0.jpg'),
('El Diablito', 'https://i.pinimg.com/474x/2b/2a/91/2b2a9108804cb536097173cbdfb1dd09.jpg'),
('La Dama', 'https://i.pinimg.com/736x/2c/c5/30/2cc530a74236874e283c5cbcdb8b9161.jpg'),
('El Catrín', 'https://i.pinimg.com/474x/52/b0/a3/52b0a3760188bbd59560780be5989a9e.jpg'),
('El Paraguas', 'https://i.pinimg.com/736x/9d/11/6a/9d116a932058bb44bc72d2548eb61eb7.jpg'),
('La Sirena', 'https://i.pinimg.com/originals/ef/3a/74/ef3a747c2c0d82b1f09586dfc769afda.jpg'),
('La Escalera', 'https://i.pinimg.com/originals/01/d5/e1/01d5e13024b79d012e5a53616aab2b0f.jpg'),
('La Botella', 'https://i.pinimg.com/474x/de/59/7d/de597d2ed908d029b6b4a110b28bd8d7.jpg'),
('El Barril', 'https://i.pinimg.com/originals/9a/c7/1e/9ac71e85797657e8b0625e0f521482d0.jpg'),
('El Árbol', 'https://i.pinimg.com/originals/38/38/80/3838801ae637def4951a665a8c7c9807.jpg'),
('El Melón', 'https://i.pinimg.com/736x/3b/9c/80/3b9c801d77870b8e750983ba1cf467b5.jpg'),
('El Valiente', 'https://i.pinimg.com/736x/d0/2f/57/d02f573857b9ad691c09caad6cf400a4.jpg'),
('El Gorrito', 'https://i.pinimg.com/736x/24/da/cb/24dacbedf1e380bb390e762294bf9ea0.jpg'),
('La Muerte', 'https://i.pinimg.com/736x/75/9c/5c/759c5c918aa27c397a31e5a6ec3bc610.jpg'),
('La Pera', 'https://i.pinimg.com/564x/81/bc/45/81bc453036459c54cdab808e7c332ebf.jpg'),
('La Bandera', 'https://i.pinimg.com/564x/0b/1d/bb/0b1dbb11cf14ff9e50dfcd4c2d5bfc84.jpg'),
('El Bandolón', 'https://i.pinimg.com/474x/ff/86/af/ff86af5648adbd8324c134d49a06d2ad.jpg'),
('El Violoncello', 'https://i.pinimg.com/236x/2e/8b/37/2e8b371643432ec3ed9d5ac579d638be.jpg'),
('La Garza', 'https://i.pinimg.com/originals/36/a0/d6/36a0d6aecdef3048d31d0de058f93886.jpg'),
('El Pájaro', 'https://i.pinimg.com/474x/33/f3/ff/33f3ffe26999f8462cce41c846509c48.jpg'),
('La Mano', 'https://i.pinimg.com/originals/3e/10/97/3e10975118d80ab7be19ca5cf7d76f74.jpg'),
('La Bota', 'https://i.pinimg.com/736x/85/4e/4a/854e4ac263061a49e8baa8a90b4c7e9b.jpg'),
('La Luna', 'https://i.pinimg.com/564x/dd/d9/78/ddd978276e48a4a4288ad811b45dd621.jpg'),
('El Cotorro', 'https://i.pinimg.com/originals/2d/29/0b/2d290b44d8da54cbfe533f6e203f0006.jpg'),
('El Borracho', 'https://i.pinimg.com/originals/fa/63/10/fa6310fd19d11a7b08f2a91f897107f8.jpg'),
('El Negrito', 'https://i.pinimg.com/736x/90/bc/f4/90bcf4d5c1b427c9bcef5e2c72b7b9d0.jpg'),
('El Corazón', 'https://i.pinimg.com/originals/b8/d1/24/b8d12434f402ecb459f04cd4524d6913.jpg'),
('La Sandía', 'https://i.pinimg.com/474x/64/4c/7b/644c7b0317aaaa4443d4083d9f7cb65b.jpg'),
('El Tambor', 'https://i.pinimg.com/originals/21/56/d0/2156d03c9a789971ad1f9e1a5acfb6f5.jpg'),
('El Camarón', 'https://i.pinimg.com/236x/2f/d1/e8/2fd1e89c60e52827089c9c26835e7c34.jpg'),
('Las Jaras', 'https://i.pinimg.com/736x/b2/5e/b0/b25eb083a73ae1612ff4405667e756e4.jpg'),
('El Músico', 'https://i.pinimg.com/474x/c6/2d/f1/c62df1afc41249d5e315fb3d6b713d53.jpg'),
('La Araña', 'https://i.pinimg.com/originals/7b/98/d7/7b98d704af0ddfb997108a7d5698ca56.jpg'),
('El Soldado', 'https://i.pinimg.com/originals/58/a1/bd/58a1bd002ef14d34d9eea2cc705dec42.jpg'),
('La Estrella', 'https://i.pinimg.com/originals/07/7a/0b/077a0beae60a271ff00c7cc51247176c.jpg'),
('El Cazo', 'https://i.pinimg.com/originals/c1/96/e9/c196e997514fb2087cf3670e5ff9a42e.jpg'),
('El Mundo', 'https://i.pinimg.com/736x/79/bc/99/79bc99c0a5d9dfa8d6ec5f3855cf4d1c.jpg'),
('El Apache', 'https://i.pinimg.com/originals/e0/b1/99/e0b199537371569a5e642cf13ee67193.jpg'),
('El Nopal', 'https://i.pinimg.com/originals/9b/3f/36/9b3f36058aa2006cc93b2a7d3e6ccdab.jpg'),
('El Alacrán', 'https://i.pinimg.com/736x/8d/88/c4/8d88c4b65fcefba8601cd8b2e30c7847.jpg'),
('La Rosa', 'https://i.pinimg.com/originals/0a/62/7c/0a627caeb0865f310597a0162cb47eb6.jpg'),
('La Calavera', 'https://i.pinimg.com/736x/c4/9f/5f/c49f5f2015833ddb4c26328ff04332e2.jpg'),
('La Campana', 'https://i.pinimg.com/originals/d9/7d/42/d97d42de902f0865bcf5ac4922e4a657.jpg'),
('El Cantarito', 'https://i.pinimg.com/originals/41/5c/ac/415cacd07db46eaf0a4259d39acf9e62.jpg'),
('El Venado', 'https://i.pinimg.com/originals/3c/c3/75/3cc375e565dd1d7c914837cba5da8e76.jpg'),
('El Sol', 'https://i.pinimg.com/474x/b1/45/96/b145967bec1987a2dc0efe136a70d163.jpg'),
('La Corona', 'https://i.pinimg.com/originals/f3/cc/fe/f3ccfec4a5ba1ecdf65a47823f0e8b92.jpg'),
('La Chalupa', 'https://i.pinimg.com/736x/39/b1/fb/39b1fb1498d94e878b375d8818a3fd6c.jpg'),
('El Pino', 'https://i.pinimg.com/originals/d4/a9/97/d4a9974a74dd0650a76df02445079e90.png'),
('El Pescado', 'https://i.pinimg.com/originals/f4/0f/18/f40f18c2a9d6811cf9b0f2d5f25f6973.jpg'),
('La Palma', 'https://i.pinimg.com/736x/b1/b4/1f/b1b41f1e09ba0a662b90bf482583f6a4.jpg'),
('La Maceta', 'https://i.pinimg.com/736x/b4/70/64/b4706417f23515ba19a1d9e0db362368.jpg'),
('El Arpa', 'https://i.pinimg.com/originals/28/0e/67/280e6748b91f157c196b8bb766fee4dc.jpg'),
('La Rana', 'https://i.pinimg.com/736x/40/c4/3f/40c43f495c9244a2f0058e36bc23772e.jpg');
